<?php
return array(
    // 应用版本
    'app_version' => '3.2.0',
    
    // 发布时间
    'release_time' => '20220825',

    // 修订版本
    'revise_version' => '1'

);
